-- Lua Conf
function love.conf(t)
    t.title = "86itRetroGames™"
    t.version = "11.3"
    t.window.width  = 1280
    t.window.height = 800
end
